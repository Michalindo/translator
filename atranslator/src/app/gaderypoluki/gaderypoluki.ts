import { Injectable } from '@angular/core';
import { stringify } from '@angular/core/src/util';

/*
 * Translator podstawieniowy na bazie klucza 'gaderypoluki'
 * pl.wikipedia.org/wiki/Gaderypoluki
 */
@Injectable()
export class GaDeRyPoLuKi {
  private translationKey = 'gaderypoluki';

  private translationMap: { [index: string]: string };

  // konstruktor
  constructor() {
    this.translationMap = this.keyToMap(this.translationKey);
  }

  // główna metoda translacji
  translate(input: string): string {
    const out = input
      .split('')
      .map((ch) => (Object.keys(this.translationMap).find((key) => key === ch) && this.translationMap[ch]) || ch)
      .join('');
    return out;
  }


  // translacja niezależna od wielkości znaków
  translateIgnoreCase(input: string): string {
    return this.translate(input.toLowerCase());
  }

  // wykonuje translację klucza w formie napisu na mapę
  private keyToMap(input: string): { [index: string]: string } {
    const chars = this.translationKey.split('');
    const pairs: [string, string][] = [];
    for (let i = 0; i < chars.length; i += 2) {
      pairs.push([chars[i], chars[i + 1]]);
    }
    return pairs.reduce((result, pair) => {
      result[pair[0]] = pair[1];
      result[pair[1]] = pair[0];
      return result;
    }, {});
  }

  // sprawdza czy znak zostanie przetłumaczony dla danego klucza
  isTranslatable(char: string): boolean {
    return this.translationMap[char] !== undefined;
  }

  // pobiera aktualną długość klucza szyfrującego
  get keySize(): number {
    return this.translationKey.length;
  }
}

package pl.poznan.put.spio.tool;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import pl.poznan.put.spio.translator.GaDeRyPoLuKi;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class GaDeRyPoLuKiToolTest {
    private GaDeRyPoLuKi g;

    /**
     * Uporządkowanie środowiska testowego JUnit. Metoda uruchamiana tylko raz,
     * dla całej klasy testującej.
     */
    @AfterAll
    public static void cleanUpClass() {
        // not this time
    }

    /**
     * Przygotowanie środowiska testowego JUnit. Metoda uruchamiana tylko raz,
     * dla całej klasy testującej.
     */
    @BeforeAll
    public static void initClass() {
        // not this time
    }

    /**
     * Uporządkowanie środowska testowego JUnit. Metoda uruchamiana po każdym
     * przypadku testowym @Test.
     */
    @AfterEach
    public void cleanUp() {
        g = null;// odrobinę nadmiarowe zwolnienie zasobów
    }

    /**
     * Test translacji, kolejne litery do sprawdzenia.
     * Wersja parametryczna, JUnit dostarcza parami msg i expected, każdy napis to jeden przypadek.
     * Przykładowo "kot, ipt" spowoduje przypisanie msg=kog oraz expected=ipt
     */
    @ParameterizedTest
    @CsvSource({"kot, ipt", "ala, gug"})
    public void EXAMPLE_shouldTranslate3(final String msg, final String expected) {
        // given

        // when
        final String result = g.translate(msg);// translacja, wynik zapisany w 'result'

        // then
        // wersja JUnit
        assertEquals(expected, result);
        // wersja assertJ
        assertThat(result).isEqualTo(expected);
    }

    /**
     * Przygotowanie środowiska testowego JUnit. Metoda uruchamiana przed każdym
     * przypadkiem testowym @Test.
     */
    @BeforeEach
    public void init() {
        g = new GaDeRyPoLuKi();// utworzenie obiektu translatora
    }

    /**
     * Sprawdzenie czy dany znak będzie podlegał translacji.
     */
    @Test
    public void shouldCheckIsNotTranslatable() {
        // given
        final String c = "Z";

        // when
        final boolean result = g.isTranslatable(c);

        // then
        assertFalse(result);
    }

    /**
     * Sprawdzenie czy dany znak będzie podlegał translacji.
     */
    @Test
    public void shouldCheckIsTranslatable() {
        // given
        final String c = "g";

        // when
        final boolean result = g.isTranslatable(c);

        // then
        assertTrue(result);
    }

    /**
     * Sprawdzenie długości kodu do translacji.
     */
    @Test
    public void shouldGetCodeLength() {
        // given

        // when
        int size = g.getCodeLength();

        // then
        assertEquals(12, size);
    }

    /**
     * Test translacji, gdy litery nie są przewidziane w mapie translacji.
     */
    @Test
    public void shouldStayNotTranslated() {
        // given
        final String msg = "LOK";

        // when
        final String result = g.translate(msg);// translacja, wynik zapisany w
        // 'result'

        // then
        assertEquals(msg, result);
    }

    /**
     * Podany w teście argument o wartości null spowoduje rzucenie wyjątku
     * {@link NullPointerException}, który w tym miejscu jest oczekiwany.
     */
    @Test
    public void shouldThrowExceptionWhenTranslateNull() {
        // when
        assertThrows(NullPointerException.class, () -> {
            g.translate(null);// tylko translacja, powinna rzucić wyjątek
        });
    }

    /**
     * Przykładowy test sprawdzania translacji napisu.
     */
    @Test
    public void shouldTranslate() {
        // given
        final String msg = "lok";

        // when
        final String result = g.translate(msg);// translacja, wynik zapisany w
        // 'result'

        // then
        assertEquals("upi", result);// metoda sprawdzająca
    }

    /**
     * Test translacji, ignorowana wielkość liter.
     */
    @Test
    public void shouldTranslateIgnoreCase() {
        // given
        final String msg = "KOT";

        // when
        final String result = g.translateIgnoreCase(msg);

        // then
        assertEquals("ipt", result);
    }

}

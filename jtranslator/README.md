# Jtranslator
Jtranslator jest implementacją translatora gaderypoluki w Javie i Springu. Zawiera logikę biznesową w formie serwisu oraz kontroler, za pomocą którego można wywołać ten serwis.
## Technologie
* Java8 lub nowsza
* JUnit 5
* Junit 4
* Spring/SpringBoot
## Uwagi techniczne
### Preferowane IDE
IntelliJ
### Budowanie aplikacji
`mvn clean install`
### Uruchamianie testów
Z wykorzystaniem IDE.
### Uruchamianie aplikacji
Z wykorzystaniem IDE,
Przykładowe zapytanie: `http://localhost:8080/gaderypoluki/napisTestowy`